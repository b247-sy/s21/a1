console.log('Hellow Wurld?');

// [SECTION] Arrays

	// Arrays are used to store multiple related values in a single variable.
	// The are declared using square brackets ([]) also known as "Array Literals"

	/*
	Syntax:
		let/const arrayName = [elementA, elementB, elementC,.....];
	*/


	let grades = [98.5, 94.3, 89.2, 90.1];
	let computerBrands = ['Acer', 'Asus', "Lenovo", 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

	// Possible use of an array but is not recommended
	let mixedArr = [12, 'Asus', null, undefined, {}];


	console.log(grades);
	console.log(computerBrands);
	console.log(mixedArr);

		// Alternative way to write arrays

		let myTasks = [
				'drink html',
				'eat javascript',
				'inhale css',
				'bake sass'
			];

		console.log(myTasks);

		let city1 = 'Tokyo';
		let city2 = 'Manila';
		let city3 = 'Jakarta';

		let cities = [city1, city2, city3];

		console.log(cities);

// [SECTION] Length Property
	
	// The .length property allows us to get the set the total number of items in an array.

		console.log(myTasks.length);
		console.log(cities.length);

		let blankArr = [];
		console.log(blankArr.length);


		// Length property can also be used with strings.
			let fullName = 'Sir Carlo';
			console.log(fullName.length);

		// Length property can also set the total number of items in an array, meaning we can actually delete the last item in the array or shorthen the array simple updating the lenght property of an array.

				// Prior to this code, myTasks.length = 4
			myTasks.length = myTasks.length-3;
			console.log(myTasks.length);
			console.log(myTasks);

			// Another example using decrementation:
			cities.length--;
			console.log(cities);

			let theBeatles = ['John', 'Ringo', 'Paul', 'George'];
			theBeatles.length++
			console.log(theBeatles);

// [SECTION] Reading from Arrays
	
	// Accessing array elements is one of the more common tasks that we do with an array
		/*
		Syntax
			arrayname[index]
		*/

		console.log(grades[3]);
		console.log(computerBrands[2]);

		console.log(grades[20]); // Will result to undefined

		let lakersLegends = ['Kobe', 'Shaq', 'Lebron', 'Magic', 'Kareem'];
		console.log(lakersLegends[1]); // Result will be Shaq
		console.log(lakersLegends[3]); // Result will be Magic

		let currentLaker = lakersLegends[2];
		console.log(currentLaker);

		// We can also reassign array values usig the items' indices

		console.log('Array before reassignment');
		console.log(lakersLegends);
		lakersLegends[2] = 'Pau Gasol';
		console.log('Array after reassignment');
		console.log(lakersLegends);

	// [SUB-SECTION] Accessing the last element of an array

		let bullsLegends = ['Jordan', 'Pippen', 'Rodman', 'Rose', 'Kukoc'];

			console.log(bullsLegends.length); // Result will be 5

		let lastElementIndex = bullsLegends.length - 1;

		console.log(bullsLegends[lastElementIndex]);

			// We can also add it directly:
			console.log(bullsLegends[bullsLegends.length-1]);

			console.log(bullsLegends[-1]); // Will result to undefined

	// [SUB-SECTION] Adding Items into the Array

		let newArr = [];
		console.log(newArr[0]);

		newArr[0] = 'Cloud Strife';
		console.log(newArr);

		newArr[1] = 'Tifa Lockhart';
		console.log(newArr);

			newArr[newArr.length] = 'Barrett Wallace';
			console.log(newArr);

// [SECTION] Looping over an Array

	// You can use a for loop to iterate over all items in an array.

			console.log(newArr.length);

		for(let index = 0; index < newArr.length; index++){
			console.log(newArr[index]);
		};

	
	// Given an array of numbers, you can also show if the following items in the array are divisible by 5 or not. You can mix in an if-else statement in the loop:

	let numArr = [5,12,30,46,40];

	for(let index = 0; index < numArr.length; index++){

		if(numArr[index] % 5 === 0){
			console.log(numArr[index] + ' is divisible by 5');
		} else {
			console.log(numArr[index] + ' is not divisible by 5');
		};
	};

// [SECTION] Multidimensional Arrays

	// Multidimensional arrays are useful for storing complex data structures

	let chessBoard = [
			['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
			['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
			['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
			['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
			['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
			['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
			['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
			['a8', 'b7', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8'],
		];

	console.log(chessBoard);

		// Accessing elements of a multidimensional arrays
			console.log(chessBoard[1][4]); // Result will be

			console.log('Pawn moves to: ' + chessBoard[1][5]);
